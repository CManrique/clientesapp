angular.module("ClientesApp")
    .factory("ClienteResource", function($resource) {
        return $resource("http://localhost:8080/api/clientes/:id", { id: "@id" }, { update: { method: "PUT" } });
    })