angular.module("ClientesApp", ["ngRoute", "ngResource"])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: "template/home.html",
                controller: "MainController"

            })
            .when('/formulario', {
                templateUrl: "template/formulario.html",
                controller: "NewClientController"
            })
            .when('/formulario/:id', {
                templateUrl: "template/formulario.html",
                controller: "LoadClientController"
            })



    })