angular.module("ClientesApp")

.controller("MainController", function($scope, ClienteResource) {
        $scope.loading = true;

        // Eliminar
        $scope.clientes = ClienteResource.query();
        $scope.removeCliente = function(cliente) {
            ClienteResource.delete({
                id: cliente.id
            }, function() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'info',
                    title: '¡Cliente ' + '"' + cliente.nombre + '"' + ' eliminado correctamente!',
                    showConfirmButton: false,
                    timer: 2000
                });
            });
            $scope.clientes = $scope.clientes.filter(
                function(element) {
                    return element.id !== cliente.id;
                });
        }

        // Cargar
        $scope.clientes = ClienteResource.query(function(data) {
            $scope.clientes = data;
            $scope.loading = false;
        }, function(error) {
            $scope.loading = false;
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: '¡No se encontraron resultados! ' + error,
                showConfirmButton: false,
                timer: 2000
            });
        })
    })
    .controller("LoadClientController", function($scope, ClienteResource, $routeParams) {
        $scope.title = "Editar Cliente";
        $scope.cliente = ClienteResource.get({
            id: $routeParams.id
        });

        //editar
        $scope.editClient = function() {
            ClienteResource.update({
                id: $scope.cliente.id
            }, {
                cliente: $scope.cliente
            }, function(cliente) {
                console.log(cliente);
            })
        };

    })
    .controller("NewClientController", function($scope, ClienteResource, $location) {
        $scope.title = "Crear Cliente";

        $scope.cliente = {};

        //crear
        $scope.saveClient = function() {
            ClienteResource.save(
                $scope.cliente,
                function(cliente) {
                    console.log(cliente);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Cliente ' + cliente.nombre + ' creado correctamente!',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $location.path("/");
                },
                function(error) {
                    $scope.loading = false;
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: '¡Error al crear!' + error,
                        showConfirmButton: false,
                        timer: 2000
                    });

                })
        };
    });